<?php get_header(); ?>

	<main role="main">
		<section class="block first_block">
			<div>
			<h1>Osallistumisen pääotsikko</h1>
			<p class="first_block__paragraph">
			Lyhyt kuvaus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed posuere interdum sem. Quisque ligula eros ullamcorper quis, lacinia quis facilisis sed sapien. Mauris varius diam vitae arcu.
			</p>
						<button class="CTA_button">Osallistu CTA</button>

			</div>
			
			<div>
			<img src="<?php echo get_template_directory_uri(); ?>/img/tytto.jpg" alt="tyttö istuskelee ikkunassa" class="first_section_image">
	
			</div>
		</section>
		<section class="block second_block">
			<div>
			<h2>Kilpailun kuvat otsikko</h2>
<p>Aikaa osallistua ja äänestää 1.2.2020 asti</p>
			</div>
			<div>
<!-- article -->


			</div>
		</section>
		<section class="block third_block">
			<div>
				<div><h2>Osallistu kilpailuun</h2></div>
			
			<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed posuere interdum sem. Quisque ligula eros ullamcorper quis, lacinia quis facilisis sed sapien. 
</p> <p> Mauris varius diam vitae arcu. Sed arcu lectus auctor vitae, consectetuer et venenatis eget velit. Sed augue orci, lacinia eu tincidunt et eleifend nec lacus. Donec ultricies nisl ut felis, suspendisse potenti. Lorem ipsum ligula ut hendrerit mollis, ipsum erat vehicula risus, eu suscipit sem libero nec erat. Aliquam erat volutpat.</p> <p>
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed posuere interdum sem. Quisque ligula eros ullamcorper quis, lacinia quis facilisis sed sapien. </p>
			</div>
			<div class="form">
			<form>
				<label for="img-header">Kuvan otsikko</label>
				<input type="text" id="img-header">
				<label for="name">Nimi *</label>
				<input type="text" id="name">
				<label for="email">Sähköposti *</label>
				<input type="email" id="email">
				<label for="file" id="file-label">Valitse kuva</label>
				<input type="file" id="file"  > 
				<div>				<input type="checkbox" id="checkbox" >
  <label for="checkbox">Hyväksyn kilpailun <a href="#">säännöt ja ehdot</a> </label></div>
  <button class="CTA_button">Osallistu</button>


			</form>
			</div>
		</section>
		<section class="block fourth_block">
			<div>
			<h3>Säännöt ja ehdot</h3>
	<p>Kilpailun aika 1.1.2020 - 1.2.2020</p>
	<p>Kilpailun järjestää Kansallisteatteri</p>
	<p>Lorem ipsum dolor sit amet</p>
	<p>Yms.</p>
			</div>
			<div>
				<p>
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed posuere interdum sem. Quisque ligula eros ullamcorper quis, lacinia quis facilisis sed sapien. Mauris varius diam vitae arcu. Sed arcu lectus auctor vitae, consectetuer et venenatis eget velit. Sed augue orci, lacinia eu tincidunt et eleifend nec lacus. Donec ultricies nisl ut felis, suspendisse potenti. Lorem ipsum ligula ut hendrerit mollis, ipsum erat vehicula risus, eu suscipit sem libero nec erat. Aliquam erat volutpat. Sed congue augue vitae neque. Nulla consectetuer porttitor pede. Fusce purus morbi tortor magna condimentum vel, placerat id blandit sit amet tortor. 
</p>
<p>
Mauris sed libero. Suspendisse facilisis nulla in lacinia laoreet, lorem velit accumsan velit vel mattis libero nisl et sem. Proin interdum maecenas massa turpis sagittis in, interdum non lobortis vitae massa. Quisque purus lectus, posuere eget imperdiet nec sodales id arcu. Vestibulum elit pede dictum eu, viverra non tincidunt eu ligula.
</p>
<p>
Nam molestie nec tortor. Donec placerat leo sit amet velit. Vestibulum id justo ut vitae massa. Proin in dolor mauris consequat aliquam. Donec ipsum, vestibulum ullamcorper venenatis augue. Aliquam tempus nisi in auctor vulputate, erat felis pellentesque augue nec, pellentesque lectus justo nec erat. Aliquam et nisl. Quisque sit amet dolor in justo pretium condimentum. </p>

<p>
Vivamus placerat lacus vel vehicula scelerisque, dui enim adipiscing lacus sit amet sagittis, libero enim vitae mi. In neque magna posuere, euismod ac tincidunt tempor est. Ut suscipit nisi eu purus. Proin ut pede mauris eget ipsum. Integer vel quam nunc commodo consequat. Integer ac eros eu tellus dignissim viverra. Maecenas erat aliquam erat volutpat. Ut venenatis ipsum quis turpis. Integer cursus scelerisque lorem. Sed nec mauris id quam blandit consequat. Cras nibh mi hendrerit vitae, dapibus et aliquam et magna. Nulla vitae elit. Mauris consectetuer odio vitae augue.
</p>
				</p>
			</div>
		</section>
	</main>



<?php get_footer(); ?>
